Many discussions about breaking the top-speed record ignore the human factor.\mmr{Topic sentence} Driving at 300 m.p.h. is a fearsome prospect. Most things travelling that fast are flying. A fully laden Boeing 747 takes off at about 185 m.p.h. For a car to safely conduct a top-speed attempt, it and the road must be in virtually perfect condition. Burst a rear tire and the front of the car will lift; lift the front and the car becomes a plane. It takes a certain kind of person to keep his foot pressed to the floor.

Andy Wallace, a soft-spoken former race-car driver in his sixties, who is now one of Bugatti's so-called test pilots, may understand high-speed driving better than anybody else. He drove the McLaren F1 that hit 240.1 m.p.h. in 1998, breaking the world record at the time; he also drove the Bugatti model in the top-speed run in 2019.

``Nothing quite prepares you for three hundred,'' Wallace told me recently. ``It's funny---as a racing driver, your brain is used to doing two hundred, two-ten. If you add another ten per cent on that, you're in a place where you're not familiar\ldots. It's all whizzing by. You keep doing that up to over three hundred. You're looking around and thinking, Gee, this is really fast!''

Bugatti tested the Chiron Super Sport multiple times, in wind tunnels and at Ehra-Lessien, before making the top-speed attempt. In the days before the 2019 run, Wallace drove several times at about 280 m.p.h. while engineers measured the downforce on various parts of the car---insuring\mmr{promising}, in his words, that ``it would stick to the road.''

\begin{figure*}[thb!]
  \centering
  \includegraphics[width=\linewidth]{cars4}
  \caption{Nobody makes better electric hypercars than Mate Rimac, a thirty-five-year-old Croatian with a thick, dark beard and a boyish manner. The electric-vehicle company he founded, Rimac, took control of Bugatti in 2021.}
  \label{fig:cars4}
\end{figure*}

Wallace explained that, at very high speeds, things had happened to the Bugatti that he did not anticipate. At about 280 m.p.h., the gyroscopic effect\mmr{ability (tendency) of the rotating body to maintain a steady direction of its axis of rotation} of the wheels turning so fast overcame the front-suspension\mmr{the parts of a vehicle that connect the body to the tires and allow the vehicle to move more smoothly over uneven surfaces} geometry, which keeps the car straight. In other words, once the car began turning in one direction it kept turning\mmr{start spinning}. He likened the effect to a spinning top. At high speeds, these tiny changes made him feel as if the wheels were ``off all over the place.'' Sometimes a crosswind buffeted\mmr{hit with great force} the car, and he had to make adjustments to avoid hitting the track barriers.

Bugatti's engineers \phr{attended to}\mmr{deal with} the steering issues. The track was scoured for pebbles\mmr{小石头}. Bugatti estimated that, at 300 m.p.h., each tire was being subjected to a seven-ton tearing force. The test car's tires were checked and double-checked by an X-ray machine. Once Wallace and the engineers felt that they'd dealt with every issue, they made the attempt. ``Things can still go wrong,'' he said. ``But if you never took a risk you'd never cross the road.''

The top-speed run took place on a cloudy Friday morning. Wallace came out of a banked\mmr{this part of road is higher than other parts} turn onto the long straightaway, at about 160 m.p.h., and ``pinned it.''\mmr{stayed at that speed} He was travelling at 278 m.p.h. when a small join in the track caused the Bugatti to momentarily jump. When it landed, it ``didn't swerve\mmr{change direction suddenly} as much as I was expecting,'' Wallace recalled. ``I thought, Great!'' Acceleration past 280 m.p.h. was relatively slow, because of the volume of air that the car had to push out of the way. Wallace was committed to keeping his foot \phr{slammed on}\mmr{pressing down hard} the accelerator, but he was consuming the straightaway alarmingly fast.

``The other end's coming now, and you can see it coming,'' he recalled, lost in the story. He needed to decelerate to 135 m.p.h. to avoid crashing at the next turn. He had planned to release the accelerator when passing a raised gantry\mmr{道路的路标架} at the side of the track, but that marker was predicated on earlier runs, when he'd driven more slowly. It would take him longer to slow down if he was actually breaking 300 m.p.h.

He hit 304.7724 m.p.h.\mmr{Note how this short sentence stands out and impresses readers}

``It seemed a shame\mmr{pity} to lift off the accelerator, but then you see the wall coming,'' Wallace said. He couldn't apply the brakes---the force going through the car was too great---but he felt that he was decelerating sufficiently to make the turn. With the bend\mmr{the turn} very close, however, he saw the speedometer showing 225 m.p.h. Wallace told himself, ``Shit, this is not good,'' before ``humping on the brakes.''\mmr{猛踩刹车} He held the car on the road through the turn, just barely, and a few minutes later he returned to a hero's welcome by the pits\mmr{an area beside a racetrack used for servicing cars during a race}.
