\dropf{A}{} few weeks later, I met von Koenigsegg at the entrance to his factory, in Ängelholm, Sweden, about an hour's drive north of Malmö. The company operates out of several hangars\mmr{a building for keeping aircratfs} on the site of a \mkln{ln:xxx}{disbanded} fighter-pilot unit of the Swedish Air Force. Koenigsegg uses the emblem of the old unit, a cartoon ghost, on its cars. Test-drives take place on an adjacent airfield. Near the complex's\mmr{the building of Koenigsegg} front door, visitors are greeted by a collection of Koenigseggs: past, present, and future.\mmr{Space description}

One car on display is a Gemera, the world's first four-seater hypercar. The Gemera, a \textsc{PHEV}---plug-in-hybrid electric vehicle---with a maximum twenty-three hundred horsepower, can accelerate from zero to sixty in less than two seconds. The first models are to be delivered to customers by the end of 2024. The idea of packing one's kids into the back of a family hypercar is amusing, and somewhat horrifying. It would certainly enliven a school commute\mmr{make a ride to school vivid and unforgettable}.

\begin{figure*}[thb!]
  \centering
  \includegraphics[width=\linewidth]{GemeraFace}
  \caption{The Koenigsegg Gemera is a limited production four-seat plug-in hybrid grand tourer to be manufactured by the Swedish automobile manufacturer Koenigsegg. \wiki{https://en.wikipedia.org/wiki/Koenigsegg_Gemera}}
  \label{fig:gemera}
\end{figure*}


At von Koenigsegg's invitation, I stuffed my six-feet-five frame into the back of the Gemera.\mmr{managed to sit into the back} It was reasonably comfortable. I also noticed cup holders, which traditional sports-car manufacturers consider heretical\mmr{abnormal and disliked}. The Gemera has eight: four chilled, four heated. ``Ferrari never had cup holders---we always had cup holders,'' von Koenigsegg told me. ``They think you should not drink while you're driving a Ferrari, and I'm, like, `Maybe you're thirsty?' ''\mmr{What if when you feel thirsty?}

\tap{narration, chronically}
He recounted the company's origins. When he was growing up, Sweden's biggest car manufacturers were Volvo and Saab. Few children have ever fantasized about owning a Volvo or a Saab. Von Koenigsegg's bedroom was decorated with posters of Lamborghinis. As a teen-ager, he developed the ambition to make ``an exciting sports car,'' and this desire endured into adulthood. In 1994, at the age of twenty-two, with money he'd made trading stocks and commodities, he launched what he then called ``the Koenigsegg project.''


Von Koenigsegg said, ``When I got into it, I realized, I don't have the resources to set up a big production plant. But I have the resources to hand-build things. Now, that's a lot of hours per car, and that's very expensive. How will I make someone pay a lot of money for one of the cars? Well, it has to be something truly special.'' He manufactured hypercars, he explained, ``out of necessity'': ``The only path was to make the cars super exciting.''


\begin{figure*}[thb!]
  \centering
  \includegraphics[width=\linewidth]{cars2}
  \caption{Andy Wallace, a soft-spoken former race-car driver in his sixties, is now one of Bugatti's so-called test pilots. He was driving the Bugatti that hit 304.8 m.p.h. Wallace recalled being on the test track: ``It seemed a shame to lift off the accelerator, but then you see the wall coming.'' Photographs by Rafał Milach / Magnum for The New Yorker}
  \label{fig:cars2}
\end{figure*}

The company sold no cars in its first eight years, although it made an impressive-looking prototype in 1996, using an Audi V-8 engine. For much of that period, the startup was based in an old thatch-roofed farmhouse, and just keeping the lights on was a struggle. The company had no income, so von Koenigsegg lived in basic accommodations near the farmhouse and took no salary, instead continuing to trade stocks to support himself and a handful of employees, including his wife, Halldora.\mmr{Sounds like he didn't have a rich dad} In 1995, his father, Jesko von Koenigsegg, who had sold a small electrical company, invested most of his savings in his son's business, and spent some time sleeping on his son's floor.\mmr{Not that rich, though, but still helpful} In 2003, part of the farmhouse burned down. The company moved to the abandoned airfield\mmr{``site of disbanded Air Force'' in  \refln{ln:xxx}{}}. Christian von Koenigsegg, who now has two sons, one of whom works for the company, told me that, without his father's support, ``we probably wouldn't have made it.'' In 2020, on his father's eightieth birthday, Christian presented him with a Koenigsegg model named for him: the Jesko. Six hundred and fifty people now work for Koenigsegg, and its cars sell out almost the moment they're announced.

Von Koenigsegg has benefitted from a change in the habits of owners. ``When we started delivering cars, in 2002, maybe a customer had one other nice sports car,'' he said. ``Two or three was pretty extreme. Now forty or fifty cars isn't unusual.'' Hypercars, he noted, have become their own investment class, given that coveted models gain in value. James Banks, a hypercar dealer, told me that there was a solid wealth-management strategy behind the trend. In many countries, though not in the U.S., automobiles are considered a ``wasting asset,'' whatever their profitability in the secondhand market; hypercars are therefore ``a very good store of wealth, because there's generally no capital-gains tax when you sell.''

The hand-built ethos still prevails at Koenigsegg. Unlike most manufacturers, which outsource auto-body parts to other companies, Koenigsegg makes nearly everything itself, from transmissions to wheels. When I asked von Koenigsegg which parts of his cars were made by other manufacturers, he paused, then said, laughing, ``The windshield wiper on the Jesko.'' He also admitted that the Gemera's brakes came from a British firm, AP Racing.

Von Koenigsegg led me around the factory. At one point, we passed a man polishing a small car part that was unidentifiable to me. Von Koenigsegg explained that it was a section of a steering wheel, and noted that, before his employees assemble a car, they spend three hundred and fifty hours polishing components. In another area of the factory, he showed me an engine that resembled a giant metallic\mmr{made of metal} heart. A few mechanics were tinkering with a valve\mmr{阀}. ``We have the highest output-per-litre engine in the world,'' von Koenigsegg said. ``A five-litre engine with sixteen hundred horsepower. It's crazy.''


\marginpar{
  \captionsetup{type=figure}
  \includegraphics[width=\linewidth]{domepistonset62mm}
  \caption{62MM DOME PISTON SET (TO BE USED WITH 150 HEAD), \src{https://www.yuminashi.eu/175cc-62mm-dome-piston-set-13mm-pin-to-be-used-with-150-head/}{yuminashi.eu}}
  \label{fig:domepiston}
}

\marginpar{
  \captionsetup{type=figure}
  \includegraphics[width=\linewidth]{combustionchamber}
  \caption{The basic diagram of a combustion chamber, \src{https://www.pinclipart.com/pindetail/iTwioih_combustion-engine-knocking-clipart/}{pinclipart.com}}
  \label{fig:combustion}
}
It did seem crazy. (Bugatti draws similar horsepower from the Chiron's engine, which is eight litres.) To achieve extraordinary numbers, von Koenigsegg excitedly explained, he had made hundreds of refinements to the standard design of a gasoline engine: ``It's how the intake is designed, how the injectors are positioned, how strong the block has to be, how strong the gaskets and head studs are. It's the porting\mmr{接口}, it's the camshaft\mmr{凸轮轴} profiles, it's the shaping of the piston dome, it's the combustion chamber in the cylinder head, it's software programming, it's ion\mmr{离子}-sensing coil-on plugs\ldots ''

Von Koenigsegg continued \phr{in this vein}\mmr{in this style/mood}. He enjoyed discussing his work. At several points during the tour, he stopped to give disquisitions\mmr{detailed and long explanations} on why his electrical inverters, or chassis-testing facilities, or over-the-air software updates\mmr{\twiki{https://en.wikipedia.org/wiki/Over-the-air_update} An \textbf{over-the-air update} (or \textbf{OTA update}), also known as \textbf{over-the-air programming} (or \textbf{OTA programming}), is an update to an embedded system that is delivered through a wireless network, such as Wi-Fi or a cellular network.} were superior. These explanations were all buttressed\mmr{supported} by a raft of statistics\mmr{a great amount of statistics}. Listening to von Koenigsegg describe his cars was like standing beneath a waterfall.\mmr{Koenigsegg talks in an unstoppable fashion with massive information like a waterfall} As a designer, he had a relentless curiosity and a willingness to jettison\mmr{reject or get rid of} old ideas. There were no gears in the Regera; in the Jesko, there were nine. He spoke of his weighty responsibilities as a business owner and a manager, and how it \phr{ate into his time}\mmr{management costs his much time which he could've used for creating cars} to create. His home sauna, he said, remained the best place to think.

As we surveyed a production line of cars in various stages of undress, von Koenigsegg discussed the demanding nature of his customers, who often requested bespoke\mmr{custom} details. Upholstering\mmr{cover} a car with ostrich\mmr{鸵鸟} leather was particularly expensive, he said, and not only because of the material's cost. In order to be ``homologated''---authorized for use on public roads---a new material had to be tested against seat sensors for at least two months. Von Koenigsegg said that an ostrich-leather upgrade might cost ``a couple hundred thousand euros.''

``We like that people customize, in a way\mmr{sometimes}, because it makes the car more unique,'' he went on. ``And it's kind of an industry standard. At the same time, it's always a lot of work for us and a hassle. So we're not unhappy if they don't do it, because it makes our life a little more livable. So, for a customization, we put a number on paper that seems very, very high\mmr{charge for a great deal of money for customization}. And it is.''
