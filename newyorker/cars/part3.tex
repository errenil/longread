\dropf{T}{}he next morning, I met the owner of the purple Koenigsegg Regera, who had agreed to drive me into the mountains. He did not wish to be named in this article, but he was happy enough for me to describe him in general terms: a businessman in his forties who split his time between Singapore and Switzerland. In the heat, he wore white linen shorts, a cream-colored polo shirt, loafers\mmr{乐福鞋}, and Louis Vuitton sunglasses. He proved to be genial\mmr{cheerful and pleasant} company, occasionally pausing from discussing his passions---which included luxury-watch design---to \gls{drag on} a red vape pen\mmr{吸红色的水烟}. As we joined an S.O.C. convoy that was leaving the hotel, he told me about his car.

The purple Regera was one of three Koenigseggs he owned. He had bought this one, for three million dollars, in 2021. It was now worth about four million, he said. The insurance premiums on hypercars are so high that he surrenders the license plates on his Koenigseggs for most of the year. He keeps the vehicles in a secure garage maintained by a Koenigsegg dealer in Switzerland, insuring\mmr{provide insurance for sth} the cars only for the few days he wants to drive them. (Insurance for S.O.C. Spain 2023, he said, had cost him about two thousand dollars.) He recognized that hypercars were an expensive pastime, but he was prudent in other ways: he never took private jets, and he thought it absurd to spend millions of dollars on a yacht. ``It's a question of taste,'' he said. ``What you and I might consider a massive waste of money gives others pleasure.''

There were only eighty Regeras in the world, and none of them had the same purple exterior\mmr{surface} as his. He'd named the car Loki, for the shape-shifting Norse god, and he wanted the color to vary in hue depending on the light. Koenigsegg, he said, had developed a new type of \gls{iridescent}\mmr{changing colors} purple paint just for him. The car's interior was, per its owner's instructions, decorated with bright-orange leather. The colorway drew admiring comments from many other S.O.C. drivers.%
\marginpar{
  \captionsetup{type=figure}
  \includegraphics[width=\marginparwidth]{scarlett}
  \caption{Four Weddings And A Funeral (1994); \src{https://www.eyeforfilm.co.uk/review/four-weddings-and-a-funeral-film-review-by-josh-morrall}{eyeforfilm.co.uk}}
  \label{fig:mclaren765lt}
}%
(The purple-and-orange scheme reminded me of a dress that Scarlett wears\mmr{\url{https://www.bonhams.com/auctions/15337/lot/236/}} in ``Four Weddings and a Funeral''; the Regera owner couldn't remember that part of the movie.)

% \begin{figure}[thb!]
%   \centering
%   \includegraphics[width=\linewidth]{carscartoon1}
%   \makebox[\linewidth][c]{\textit{``Maybe sometime you could come over to \uline{my} piling?''}}
%   \makebox[\linewidth][c]{\footnotesize\textsf{\textcolor{tnygray}{Cartoon by Lonnie Millsap}}}
% \end{figure}

The convoy entered the mountains. The Mediterranean twinkled beneath us. Most of the roads were two-lane highways, and there were few opportunities to test the Regera's power, since regular drivers blocked the way. Occasionally, we accelerated with frightening ease past another car.\mmr{Next sentence \emph{shows} how ``frightening'' it is} On one short uphill overtake, I noticed the speedometer cross 180 kilometres per hour (112 m.p.h.); after we'd slowed down, I also noticed that I'd begun to sweat.

During an early-morning briefing at La Zambra, a Spanish race-car driver who'd been hired as an adviser for the weekend told attendees what to expect from the local traffic police. The implicit message was that an understanding had been reached. (``We have no problem---but don't do crazy things.'')%
\marginpar{
  \captionsetup{type=figure}
  \includegraphics[width=\marginparwidth]{FiatUno}
  \caption{Fiat Uno; \wiki{https://en.wikipedia.org/wiki/Fiat_Uno}}
  \label{fig:fiatuno}
}%

On the morning's drive, whatever deal was in place seemed to hold: no hypercars were ticketed for speeding. At one on-ramp, I spotted a piddly Fiat Uno that had been stopped by a police car. Its owner gazed ruefully\mmr{regretfully} at the S.O.C. convoy roaring past.

I asked the Regera owner if he knew what the Spanish speed limit was. ``No fucking idea,'' he said.

Later that morning, a police vehicle appeared shortly after the Regera owner did what might be termed\mmr{called, described as} a crazy thing: overtaking another car on a two-lane road, with traffic coming from the other direction fast enough that I momentarily forgot to breathe. ``That was easy,'' he reassured me. Then he asked if I spoke Spanish, so that I could \gls{smooth-talk}\mmr{向\ldots 说说好话} local officers if they pulled us over.

As the Regera whipped along, people in other vehicles hung phones out of their windows to take photographs. I suggested that this must be part of the appeal of owning such a car. ``I actually don't like the attention it gets,'' the Regera owner said. ``I like this car because it's like no other. It's a unique driving experience.'' For one thing, he said, the car had no gears. Koenigsegg had developed a ``direct drive'' technology that obviated the need for a mechanical gearbox, and the system was first installed in the Regera. The car's five-litre engine, along with three electric motors, resulted in instant, unyielding \gls{torque}\mmr{扭矩}---the rotational force that translates into acceleration. When the car sped up, I felt as if I'd been suctioned\mmr{pulled with force} to the seat by a giant vacuum cleaner. Despite having no transmission, the car had some seventeen hundred horsepower at its disposal. The litter-strewn VW S.U.V. that I drive has about ten per cent of that power.

The farther our convoy ventured from La Zambra, the bolder the cars in our group became. A baby-blue Ferrari Daytona\mmr{The city of Daytona Beach, Florida.  Famous for a couple of famous car races are held there.} SP3 sped around us. The car was owned, I later learned, by a Dutch real-estate magnate with conservative views on criminal sentencing.

\begin{figure*}[tbh!]
  \centering
  \includegraphics[width=\linewidth]{FerrariDaytonaSP3}
  \caption{Ferrari Daytona SP3: A Sports Prototype Soul \src{https://www.ferrari.com/en-EN/auto/ferrari-daytona-sp3}{ferrari.com}}
  \label{fig:ferraridaytona}
  \end{figure*}

``For some people here, it's a dick-measuring contest,'' the Regera owner said. He'd never seen the appeal of owning a Ferrari. The cars themselves were nice, he said, but he \mkln{ln:213}{disliked} the company, which forced customers to buy several lower-value Ferraris before they could even be considered for more exclusive models.\mmr{Being rich alone is not enough to own a Ferrari Daytona.} Pointing at the Daytona SP3, he said, ``You'd need a thirty-million-dollar purchase history to buy that car.''
