% Part 1
\begin{minipage}{\linewidth}
    \begin{center}
    {\textcolor{tnyred}{\tiny\uppercase{a reporter at large}}}\\
    \vspace{1.5em}
    {\large\uppercase{alice munro's passive voice}}\\
    \vspace{0.5em}
    {\small\emph{Alice Munron's partner sexually abused her daughter Andrea. The harm ran through the work and the family.}}\\
    {\tiny\textsf{By \href{https://www.newyorker.com/contributors/ed-caesar}{Rachel Aviv}}}\\
    {\tiny\textsf{December 23, 2024}}
  \end{center}
\end{minipage}

\begin{figure}[th!]
  \centering
  \includegraphics[height=8cm]{cover_andrea}
  \caption{\sffamily Andrea was nine when the abuse began. She later wrote that Alice ``loves and protects the most destructive person of my life.'' \textcolor{tnygray}{Photograph by Andrea Modica for The New Yorker}}
  \end{figure}

\dropf{``I}{} am a writer or used to be a writer,'' Alice Munro wrote in 2014, in one of the last stories she tried to compose. A year earlier, she had won the Nobel Prize in Literature. But she had Alzheimer's and had been in decline\mmr{become sicker, less powerful, etc} for several years. Her partner of four decades, Gerald (Gerry) Fremlin, had recently died, and she was living near her daughter Jenny, in Port Hope, east of Toronto. ``I'm a writer, as I said, and I suppose that sticks for a while even though you don't \sout{due do} due\mmr{Alice is too old to pronounce/write `do' properly} it anymore,'' she wrote, in shaky longhand. ``I am going to write what happened yesterday, though at first I did not mean to, didn't think of it, as I don't anymore.''\mmr{``as I don't [generally] write down events anymore.'' This is particularly moving because Munro was known for transforming everyday experiences into literary art throughout her career. The passage suggests that in her advanced age, she has stopped not just the act of writing, but even the instinctive writerly impulse to mentally frame experiences as potential material for writing. It's a reflection of how deeply ingrained her identity as a writer was, and how that part of her has faded.}

The day before, Alice had been waiting outside a bank while Jenny, the second of her three daughters, took care of business inside. ``My daughter does all that sort of thing now,'' Alice wrote. ``I'm sort of frightened by it.'' A man she knew vaguely from high school, in Wingham, a rural town in Ontario, came out of the bank and recognized her. Alice asked after\mmr{ask about} his two sisters, who turned out to be dead. ``Me the only bugger left on the planet,'' the man said, nodding. His words seemed to release something in Alice, and she tried to build a story around the conversation. But after several beginnings she doubted herself: ``Why, I don't know---I mean why write. Even my pen seems unwilling.'' She crumpled up the pages. Later, Jenny picked them out of the trash.

Jenny always made sure that her mother had pens and spiral-bound notebooks beside her chair, but eventually Alice became too impaired\mmr{``... had been in decline for several years''} to use them. As she lost her abilities\mmr{unable to write anymore}, Jenny noticed a change. ``Something happened where she was full of love and understanding, and people felt better after being with her,'' she told me. In the Munro family, the word ``earnest'' had been used as an insult. Once, in a letter, Alice thanked Jenny for her ``loving kindness,'' and then, as if embarrassed, drew an arrow pointing toward the phrase and added the words ``blah blah blah.''\mmr{傲矫老太} In her illness, though, Alice seemed to access her emotions more freely, a shift that Jenny attributed, in part, to the fact that she wasn't writing.\mmr{Jenny believed that Alice seemed to access her own emotions more freely partly because of the fact that ...} ``She wasn't putting every difficulty in her life through that machine that turned things into gold,'' Jenny said.\mmr{Alice Munro's writing process - how she would transform difficult life experiences into valuable literary works (like turning base metals into gold). Jenny suggests that when Alice stopped writing due to illness, she could no longer use this coping mechanism of converting painful experiences into stories, forcing her to face her emotions more directly.
}

For years, Jenny had been trying to talk with her mother about something that had been put through the machine repeatedly: the sexual abuse of Alice's youngest daughter, Andrea, by Gerry, and Alice's refusal to see the harm that it had done. ``She\mmr{Alice, my mother} loves and protects the most destructive person of my life,'' Andrea had written years earlier.

Alice used to shut down when Jenny \ann{\uline{brought up}}{提及} the subject, but after she got Alzheimer's, Jenny said, ``she didn't feel invested in that person, Gerry, at all, or in the person she'd been with him. She started to lose that great terror over the truth.''

Jenny and her mother had lucid conversations about Andrea's abuse, which Jenny sometimes recorded, but Alice would forget what had happened a few minutes later. In one conversation, in 2019, Alice exhaled loudly and said under her breath, ``How awful.'' She looked up at Jenny and said, ``It was beastly of me not to get rid of him.''\mmr{Alice did feel guilty but she did nothing to help her daughter, Andrea, out, which led to a disconnection between her and her daughter later}

``Did you sort of blame yourself and hate yourself and think Andrea would never love you again, too?'' Jenny asked, hoping for more self-reflection.

``No, I don't think it was that,'' Alice said. ``I don't know why I didn't.'' She sat in a cushioned chair, wearing a zip-up sweater, a fleece blanket\mmr{羊毛毯子} spread over her lap. Then she said in a louder voice, as if finally discovering something solid, ``Well, he told me he'd kill himself, of course.'' Gerry had said that he couldn't live without her. ``He was in a desperate situation.''

``And it's an empty threat, isn't it?'' Jenny said. ``What if Andrea had killed herself?''\mmr{Gerry might pretend to thread you but your daughter may likely commit suicide}

``Yes, exactly,'' Alice said, nodding.

``A lot of victims of child abuse do,'' Jenny said.

Alice held her hand to her forehead. She seemed to be losing track of the emotional center of the conversation. ``Does she think about it still?'' she asked.

``This?''\mmr{Andrea's thinking about killing herself} Jenny said. ``It's not something you get over.''\mmr{It's hard (for Andrea) to forget or accept something so terrible}

``Oh, God. Oh, God,'' Alice said, in a high, pained voice, bowing her head and holding it in her hand.

Jenny asked Andrea if she could share the recording with her,\mmr{if Jenny could share the recording with Andrea; if Andrea would like to hear what Jenny had talked with their mother} but Andrea wasn't interested. ``Every time I found a morsel of remorse\mmr{a small piece of feeling of guilty from her mother, Alice, towards Andrea}, I would tell Andrea,'' Jenny said. ``But it was just so little, so late.'' Andrea felt as if her mother had found a disease that was almost too convenient, a permanent forgetting. She told me, ``I was kind of mad at her, like, Oh, yeah. You found your way out.''\mmr{Andrea thinks her mother, Alice, once with Alzheimer's, had a perfect excuse to forget what happened and thus got free of it}
