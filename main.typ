// main.typ
#import "longread.typ": note, template, dropcap, ruby, ol
#show: template.with(
    column: [a reporter at large],
    title: [alice munro's passive voice],
    shorttitle: [you won't get free of it],
    subtitle: [The celebrated writer’s partner sexually abused her daughter Andrea. The abuse transformed Munro’s fiction, but she left it to Andrea to confront the true story.],
    author: "By Rachel Aviv",
    date: datetime(year: 2024, month: 12, day: 23),
)

#dropcap(
    height: 2,
    gap: 0.5em,
    hanging-indent: 0pt,
    overhang: 8pt,
    font: "Arvo",
)[*"I* am a writer or used to be a writer,” Alice Munro wrote in 2014, in one of the last stories she tried to compose. A year earlier, she had won the Nobel Prize in Literature. But she had Alzheimer’s and had been in decline#note[become sicker] for several years. Her partner of four decades, Gerald (Gerry) Fremlin, had recently died, and she was living near her daughter Jenny, in Port Hope, east of Toronto. “I’m a writer, as I said, and I suppose that sticks for a while even though you don’t due do due it anymore,” she wrote, in shaky longhand. “I am going to write what happened yesterday, though at first I did not mean to, didn’t think of it, as I don’t anymore.”]

The day before, Alice had been waiting outside a bank while Jenny, the second of her three daughters, took care of business inside. “My daughter does all that sort of thing now,” Alice wrote. “I’m sort of frightened by it.” A man she knew vaguely from high school, in Wingham, a rural town in Ontario, came out of the bank and recognized her. Alice asked after his two sisters, who turned out to be dead. “Me the only bugger left on the planet,” the man said, nodding. His words seemed to release something in Alice, and she tried to build a story around the conversation. But after several beginnings she doubted herself: “Why, I don’t know—I mean why write. Even my pen seems unwilling.” She crumpled up the pages. Later, Jenny picked them out of the trash.

Jenny always made sure that her mother had pens and spiral-bound notebooks beside her chair, but eventually Alice became too impaired to use them. As she lost her abilities, Jenny noticed a change. “Something happened where she was full of love and understanding, and people felt better after being with her,” she told me. In the Munro family, the word “earnest” had been used as an insult. Once, in a letter, Alice thanked Jenny for her “loving kindness,” and then, as if embarrassed, drew an arrow pointing toward the phrase and added the words “blah blah blah.” In her illness, though, Alice seemed to access her emotions more freely, a shift that Jenny attributed, in part, to the fact that she wasn’t writing. “She wasn’t putting every difficulty in her life through that machine that turned things into gold,” Jenny said.

For years, Jenny had been trying to talk with her mother about something that had been put through the machine repeatedly: the sexual abuse of Alice’s youngest daughter, Andrea, by Gerry, and Alice’s refusal to see the harm that it had done. “She loves and protects the most destructive person of my life,” Andrea had written years earlier.

Alice used to shut down when Jenny brought up the subject, but after she got Alzheimer’s, Jenny said, “she didn’t feel invested in that person, Gerry, at all, or in the person she’d been with him. She started to lose that great terror over the truth.”

Jenny and her mother had lucid conversations about Andrea’s abuse, which Jenny sometimes recorded, but Alice would forget what had happened a few minutes later. In one conversation, in 2019, Alice exhaled loudly and said under her breath, “How awful.” She looked up at Jenny and said, “It was beastly of me not to get rid of him.”

“Did you sort of blame yourself and hate yourself and think Andrea would never love you again, too?” Jenny asked, hoping for more self-reflection.

“No, I don’t think it was that,” Alice said. “I don’t know why I didn’t.” She sat in a cushioned chair, wearing a zip-up sweater, a fleece blanket spread over her lap. Then she said in a louder voice, as if finally discovering something solid, “Well, he told me he’d kill himself, of course.” Gerry had said that he couldn’t live without her. “He was in a desperate situation.”

“And it’s an empty threat, isn’t it?” Jenny said. “What if Andrea had killed herself?”

“Yes, exactly,” Alice said, nodding.

“A lot of victims of child abuse do,” Jenny said.

Alice held her hand to her forehead. She seemed to be losing track of the emotional center of the conversation. “Does she think about it still?” she asked.

“This?” Jenny said. “It’s not something you get over.”

“Oh, God. Oh, God,” Alice said, in a high, pained voice, bowing her head and holding it in her hand.

Jenny asked Andrea if she could share the recording with her, but Andrea wasn’t interested. “Every time I found a morsel of remorse, I would tell Andrea,” Jenny said. “But it was just so little, so late.” Andrea felt as if her mother had found a disease that was almost too convenient, a permanent forgetting. She told me, “I was kind of mad at her, like, Oh, yeah. You found your way out.”
