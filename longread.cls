\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{longread}[2024/01/12 v1.0 Class for long read notes]

\DeclareOption{blb}{\OptionNotUsed}
\DeclareOption{te}{\AtEndOfClass{\setmainfont{Halant}}}
\DeclareOption{tny}{\AtEndOfClass{\RequirePackage{ebgaramond}}}
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrbook}}
\ProcessOptions\relax

% Base class
\LoadClass[
parskip=half,
fontsize=12pt,
% chapterprefix=true,
numbers=noenddot,
bibliography=totoc]
{scrbook}

% Page layout: 1.5 column, mostly inspired by Kenohori:
% https://3d.bk.tudelft.nl/ken/en/2016/04/17/a-1.5-column-layout-in-latex.html
\RequirePackage[
includemp,
paperwidth=20cm,                % Old: 18.90cm
paperheight=24.58cm,
top=2.170cm,
bottom=3.510cm,
inner=2.1cm,
outer=2.1cm,
marginparwidth=4.2cm,
marginparsep=0.4cm]
{geometry}


% Use Chinese characters and make them smaller
% https://tex.stackexchange.com/a/45403
\RequirePackage{xeCJK}
\defaultCJKfontfeatures{Scale=0.8}
\setCJKfamilyfont{zhrm}{Source Han Sans CN}
\setCJKfamilyfont{jarm}{IPAmjMincho}

% Latin font
\RequirePackage[T1]{fontenc}
\RequirePackage{fontspec}
% \usepackage[cmintegrals,cmbraces]{newtxmath}
% \usepackage{ebgaramond-maths}
% \setmainfont{Halant}
\RequirePackage[defaultsans]{lato}
% For symbols
\RequirePackage{amsmath}
\RequirePackage{amssymb}
% \RequirePackage{latexsym}

\RequirePackage{pifont}         % Occasional use of symboles
\RequirePackage[normalem]{ulem} % Better underlines
\RequirePackage{lettrine}       % Dropping-down initial letter
\RequirePackage[
switch*,
pagewise,
modulo,
]{lineno}                       % Line numbers, every five line

% Floats, tables, margin (side-) and foot-notes
\RequirePackage{ragged2e}
\RequirePackage{marginfix}      % Make environment in margins float freely
\RequirePackage{marginnote}
\RequirePackage[footnote]{snotez} % Sidenotes and footnotes (as sidenotes)
\setsidenotes{
  text-format=\normalfont\scriptsize,
  text-format+=\RaggedRight,
  % text-mark-format=\textsuperscript{\footnotesize\fontfamily{lmodern}\selectfont#1},
  text-mark-format=\textsuperscript{\scriptsize\sffamily\textcolor{tnyblue}{#1}},
  % note-mark-format={\scriptsize\fontfamily{lmodern}\selectfont#1}:,
  note-mark-format=\scriptsize\sffamily\textcolor{tnyblue}{#1}:,
  note-mark-sep=\enskip
}
\RequirePackage{scrlayer-scrpage}       % Customise head and foot regions
% \RequirePackage[skip=1em]{parskip}

\RequirePackage[
font=scriptsize,
hypcap=true,
justification=raggedright,
skip=2pt                        % Caption 2pt below the figure
]{caption}
\RequirePackage{floatrow}       % Set up figure*/float so they take entire width
\floatsetup[widefigure]{
  margins=hangoutside,
  facing=yes,
  capposition=bottom
}

\floatsetup[figure]{
  margins=hangoutside,
  facing=yes,
  capposition=beside,
  capbesideposition={center,outside},
  floatwidth=\textwidth
}

\floatsetup[widetable]{
  margins=hangoutside,
  facing=yes,
  capposition=bottom
}
\floatsetup[table]{
  margins=hangoutside,
  facing=yes,
  capposition=beside,
  capbesideposition={center,outside},
  floatwidth=\textwidth
}

\RequirePackage{graphicx}       % pictures
\graphicspath{ {./pics/} }
\RequirePackage{subcaption}     % Subfigures
\RequirePackage[within=none]{newfloat} % Define custom float environments
% For common illustrations
\DeclareFloatingEnvironment[
placement={!htb},
name=Illustration,
]{illustr}

% For New Yorker cartoons
\DeclareFloatingEnvironment[
placement={!htb},
name={},
]{cartoon}

% More floats
\extrafloats{100}
% Font setup
\addtokomafont{caption}{\footnotesize}
% Figure captions with no indentation
\setcapindent{0pt}
\setlength\columnseprule{.4pt}

\RequirePackage{xcolor}         % Colours in text
\RequirePackage{etoolbox}       % Easy programming to modify TeX stuff
% Common colors
\definecolor{vocab}{rgb}{0.40, 0.04,0.04}
\definecolor{gnlinks}{rgb}{0.09, 0.45, 0.27}
% Economist
\definecolor{tered}{RGB}{227, 18, 11}
\definecolor{tebg}{RGB}{235, 233, 224}
% New Yorker
\definecolor{tnyred}{RGB}{219, 51, 52}
\definecolor{tnygray}{RGB}{102, 102, 102}
\definecolor{tnyblue}{RGB}{8, 121, 191}

% Ruby
\RequirePackage{pxrubrica}

% Cover image
\RequirePackage{tikz}

% Hyperlinks
\PassOptionsToPackage{hyphens}{url}
\RequirePackage[
xetex,
unicode,
breaklinks=true,
colorlinks=true,
allcolors=gnlinks,
pagebackref,
linktoc=all,
]{hyperref}  % Hyperlinks

% Glossaries loaded after hyperref as required by the package
\RequirePackage[
notranslate,
]{glossaries}
% Make glossary word in chicago-red and media bold
\renewcommand*{\glsnamefont}[1]{\textcolor{vocab}{\textmd{#1}}}


% Make page head (and foot) extends up to the space used by the marginal notes
\newlength{\overflowingheadlen}
\setlength{\overflowingheadlen}{\linewidth}
\addtolength{\overflowingheadlen}{\marginparsep}
\addtolength{\overflowingheadlen}{\marginparwidth}
\renewpagestyle{scrheadings}{
{}%
{}%
{}%
}{
  {
    \hspace{-\marginparwidth}\hspace{-\marginparsep}%
    \makebox[\overflowingheadlen][l]{\upshape\large\thepage\quad\uppercase{\footnotesize{the new yorker}}}
  }%left page
  {
    \hspace{-\marginparwidth}\hspace{-\marginparsep}%
    \makebox[\overflowingheadlen][r]{\upshape\uppercase{\footnotesize{the new yorker}}\quad\large\thepage}
  }%right page
{}%all page
}

\renewpagestyle{plain.scrheadings}{
  {}%
  {}%
  {}%
}{
  {\makebox[\overflowingheadlen][l]{\thepage}}%
  {\makebox[\overflowingheadlen][r]{hehe\upshape\thepage}}%
  {}%
}

% New commands
\newcommand{\mmr}[1]{\sidenote{#1}}% murmurs: my comments/notes
\newcommand{\tap}[1]{\marginpar{\scriptsize#1}}% techniques and purposes
\newcommand{\mkln}[2]{\hypertarget{#1}{#2}}
\newcommand{\refln}[2]{\hyperlink{#1}{Line~#2}}
\newcommand{\wiki}[1]{Source: \href{#1}{Wikipedia}}
\newcommand{\twiki}[1]{\href{#1}{Wikipedia}:} % Wiki terms
\newcommand{\src}[2]{Source: \href{#1}{#2}}
\newcommand{\net}{Source: Internet}
\newcommand{\phr}[1]{\uline{#1}}
\newcommand{\ie}{i.e.}
\newcommand{\eg}{e.g.}
\newcommand{\reffig}[1]{\hyperref[#1]{Figure}~\ref{#1}}
\newcommand{\dropf}[2]{\lettrine[lines=2]{#1}{\footnotesize\uppercase{\textbf{#2}}}}
\newcommand{\chtxt}{\CJKfamily{zhrm}\CJKnospace}
\newcommand{\jptxt}{\CJKfamily{jarm}\CJKnospace}
\newcommand{\ctnby}[2]{\caption*{\small\textit{#1}\\ \textcolor{tnygray}{\footnotesize\textsf{Cartoon by #2}}}}
\newcommand{\ann}[2]{\aruby[g]{#1}{\footnotesize{#2}}}
