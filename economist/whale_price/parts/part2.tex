% Part 2
\dropf{I}{n} 1861 \emph{\small{Vanity Fair}}, a magazine, published a cartoon showing sperm whales in black tie and ball gowns\mmr{黑蝴蝶结礼服} raising their champagne glasses in celebration and relief. It was, the caption informed readers, ``The Grand Ball given by the Whales in honour of the discovery of the Oil Wells in Pennsylvania''.\mmr{See \reffig{fig:black}. \textsf{``NO MORE FOR OUR BLUBBER''}} The oil rush which started in Titusville, Pennsylvania in 1859 had produced a new commodity that would \gls{reilluminate}\mmr{to supply\ldots\ with light} America. Light would no longer come from burning whale oil but from kerosene\mmr{煤油} gushing out of the ground. Thanks to this substitution of fossil-fuel energy for biomass\mmr{replacement of A with B}, the whale would be left at peace.

\begin{figure*}[bh!]
  \centering
  \includegraphics[width=\linewidth]{pics/grandball}
  \caption{Grand ball given by the whales in honor of the discovery of oil wells in Pennsylvania," Vanity Fair, 1861. \href{https://www.researchgate.net/figure/Grand-ball-given-by-the-whales-in-honor-of-the-discovery-of-oil-wells-in_fig35_322641749}{Wikipedia}}
  \label{fig:black}
  \end{figure*}

  Those employed in the American whaling fleet\mmr{捕鲸队}, sailing predominantly out of New Bedford in Massachusetts, were less moved to\mmr{feel less excited about} celebration. Oil had been their main product; \gls{baleen} for \gls{corsets}\mmr{(尤指旧时妇女束腰的)紧身内衣}, ambergris\mmr{龙涎香(产自抹香鲸体内的一种可作香料的蜡状物质)} for \gls{parfumiers}\mmr{a person who manufactures or retails perfume} and other ancillaries could not support the industry. From a peak of 137 whaling voyages in 1851, the year in which Herman Melville's ``Moby Dick'' was published, the number dwindled\mmr{reduced gradually} to around a dozen a year by the turn of the 20th century. Whale oil prices halved from £32 a barrel in 1874, when the data series starts, to £16 in 1887. In a piece which draws a parallel between the economic and strategic importance of the historical whaling industry and the oil industry, Charlotte Epstein, a political philosopher, notes that ``Whale oil constitutes the only form of energy that our societies both centrally depended upon and turned away from completely.''

\marginpar{
\captionsetup{type=figure}
\includegraphics[width=\marginparwidth]{baleen}
\caption{Appearance of baleen hair in a whale's open mouth. \wiki{https://en.wikipedia.org/wiki/Baleen\#/media/File:Whale_Mouth_Mivart.png}}
\label{fig:baleen}
}

\marginpar{
\captionsetup{type=figure}
\includegraphics[width=\marginparwidth,height=5cm]{mobidick}
\caption{The cover of the first American edition of \emph{Moby-Dick} by Herman Melville. Source: \href{http://beinecke.library.yale.edu/dl_crosscollex/photoneg/oneITEM.asp?pid=39002036007103&iid=3600710&srchtype=}{Beinecke Library}}
\label{fig:mobidick}
}
That final turn\mmr{from ``depended upon'' to ``turned away''}, though, was still a few twists away\mmr{讲去鲸化的最后波折}. Whales of the 20th century would have cause to look back on their forebears in \emph{\small{Vanity Fair}} swilling champagne under a banner saying ``Oil's well that ends well'' with bitter irony. Yes, fossil fuels initially made whale oil less valuable. But before long\mmr{soon} they would also make whale hunting much cheaper. Fossil fuels allowed Norwegian and British steamships to chase them into the waters around Antarctica, which Melville had called the whales' ``polar citadel''. Explosive \glspl{harpoons}\mmr{a long whale-hunting weapon}, made possible by the fossil-fuel-driven growth in explosives, made whale hunting easier and also, worst of ironies, more important for nations going to war, as whales were an excellent source of the glycerine\mmr{甘油；丙三醇; 美式拼写 glycerin} used in nitroglycerine\mmr{硝化甘油;甘油三硝酸酯}.

These new hunting technologies made the great whales a source of fat cheap enough to be used as pet food, a treatment for trench foot and, overwhelmingly, spread on bread. By 1935 roughly 84\% of the world's whale oil was being turned into margarine, at a price per barrel of £15 or so. With up to 120 barrels of oil in a typical blue whale's blubber, the largest animal to have ever lived had a market value of about £1,800 (\$130,000 or so in 2023).


\begin{figure*}[bh!]
  \centering
  \includegraphics[width=\linewidth]{pics/bluewhale}
  \caption{blue whale, Source: \href{https://cdn.britannica.com/57/73257-050-7BA1BE72/Blue-whale.jpg}{Britannica}}
  \label{fig:bwhale}
\end{figure*}


Whales are, in principle, a renewable resource. But the rate at which they renew themselves is fairly slow. Blue whales\mmr{See \reffig{fig:bwhale}} \phr{have gls{calves}}\mmr{whales to give birth to their babies} every two or three years. It takes those calves\mmr{baby whales} a couple of decades to get to their full size. This meant that as sales increased, stocks diminished, a change which first became a matter of official concern in their breached citadel: the Antarctic waters administered from Britain's South Atlantic colonies.

In 1911, Sir William Allardyce, governor of the Falkland Islands Dependencies, began to worry that the rapid expansion of this Southern Ocean whaling risked becoming unsustainable. The whales were, to all extents and purposes\mmr{in effect; essentially}, free to whoever could catch them; the government required only that the whalers pay for a whaling license—£25—and a \gls{royalty}\mmr{an amount of money paid to the owner/admin of the land} on each whale caught—£10 for a right whale (so called because they were the right whales to go after)\label{l:right}, ten shillings for a sperm whale and five shillings for any other whale. Such fees were less than 1\% of the whalers' profits.\mmr{Many prices in this paragraph indirectly show the worth of a whale then}

Had Allardyce been a modern economist he would have raised the prices. Instead he limited the number of licenses he was willing to issue. He also communicated his concerns to London. As a result the British Colonial Office developed an interest in cetology\mmr{鲸类学}. The levies\mmr{royalties} on whale hunting were used to fund Antarctic research voyages\mmr{向捕鲸者收税以支持南极科学考察} on board the \gls{refitted}\mmr{repair a ship or aircraft or to make it ready to use again} \emph{\small{RRS Discovery}}

\marginpar{
\captionsetup{type=figure}
\includegraphics[width=\marginparwidth]{rrsdiscovery}
\caption{The RRS Discovery is a barque-rigged auxiliary steamship built in Dundee, Scotland for Antarctic research. Source: \href{https://en.wikipedia.org/wiki/RRS_Discovery\#/media/File:Discoveryboat.jpg}{Wikipedia}}
\label{fig:rrs}
}%
, once the ship of polar explorer Robert Falcon Scott. Her complement of scientists\mmr{the scientists that she manages; complements of 指配额} took note of the abundance of nutrients in the waters around the islands, their currents and temperatures, the types of phytoplankton\mmr{浮游植物} (which photosynthesise\mmr{进行光合作用}, like plants) and zooplankton\mmr{浮游动物} (which eat the phytoplankton) which they were home to, the volume of krill\mmr{磷蝦} (crustaceans that eat plankton of all sorts) and the populations of whales (most of which eat the krill). To track the whales' movements they fired on them with harpoons which left stainless steel darts buried under their blubber; whalers were paid £1 for every dart they recovered.\mmr{另一种价格}

The Discovery expeditions showed an appreciation that measuring the true wealth of nations requires ecologists as well as economists: a well-run country needs to know about the natural assets on which its prosperity ultimately depends.\mmr{Topic sentence} They did not call it ``natural-capital accounting'', but armed with the expeditions' data the Colonial Office saw the need to protect the future of the country's assets\mmr{office saw the data so saw the need to protect whales}. It pressed for restrictions on catches, including limits on the hunting season, and prohibitions on the hunting of juveniles\mmr{young whales}.\mmr{这段点明科学和经济一起才能唤起保护意识并让人采取行动}

Attempts at spreading such regulations internationally, however\mmr{可事实是残酷的}, were thwarted\mmr{prevented} by politics, economics and technology. When, in 1927, the recently formed League of Nations proposed an international conference on whaling, various countries objected\mmr{disagree with the idea (global) and think it should be bilateral (between neighboring countries)} that such issues should properly be bilateral, not global. There were economic objections, too: when it decided not to hold the conference, the league said it was because the market had no need of such regulation: ``If hunting becomes unprofitable it will stop by itself, long before whales are exterminated.''\mmr{long before = (there will be) a long time before (sth will happen)}

Technology, meanwhile, was helping the industry escape almost all regulation.\mmr{Even tech backfires} In Allardyce's day whales killed at sea still had to be processed on land, or at least at a near-shore ``floating factory'', allowing some oversight. The advent of the factory ship allowed whalers to do all that they needed to do on the high seas\mmr{在大海上}, and thereby pay nothing for their catches. Overexploitation continued: whale numbers fell further. Writing in 1933, Harold Salvesen, \gls{scion}\mmr{a person born in a rich/famous/important family} of a whaling family but also, in the 1920s, an economist at the University of Oxford, explained why the simple nostrums\mmr{a suggested solution} that had apparently satisfied the League of Nations did not work. ``The reason is quite simple'', he wrote. ``Whaling companies never have property in the whales they hunt; if one company spares them not\mmr{断句在 ``not'', 鲸鱼不是捕鲸者的(不动)财产，一家公司不捕了，鲸鱼不会获利，反而另一家公司就会捕来卖钱} the whales but another company will profit from it.''
