\begin{illustr}
  \centering
  \includegraphics[width=\textwidth]{20231223_XMD020}
  \caption{KATE COPELAND\copyright}
  \label{ill:whalefood}
\end{illustr}

\dropf{T}{HE FLESH} of a great whale tastes something like beef or venison.\stt{This section focuses on whales' meat.} It is iron-rich and coloured almost purple thanks to the amount of haemoglobin\mmr{血红素} needed to store a body's worth of oxygen while diving into the abyss. In a basement restaurant in Tokyo, it is served as steak, battered like fried chicken, wrapped into shumai\mmr{烧麦}, a kind of Chinese dumpling, and finally fried as a set of well-spiced rice balls. It all tastes fine, but not quite good enough for your correspondent\mmr{your correspondent 指写作此文的记者，也即笔者} to overcome his discomfort. Most of the dishes are left abandoned after \phr{a token effort}\mmr{very little effort, 一口弃}. Eating whale comes more naturally to Japanese people, suggests Konomo Kubo, the secretary of the Japanese Whaling Association and your correspondent's dining partner.

After the IWC's 1982 moratorium Japan continued to hunt \phr{under the auspices of}\mmr{with the help and support of a particular organization or person} research. Kyodo Senpaku, the national whaling company set up in 1987, \phr{auctioned off}\mmr{拍卖掉} the meat. Initially prices increased as supply dwindled: the cost of a kilogram of whale meat rose from \$9 straight after the moratorium to around \$30 per kilo in the early 1990s, according to official auction statistics. Today it is a minority habit. In 2023 a Tokyo fishmonger sells a 130g steak for ¥702, roughly \$35 per kilo. Overall consumption has collapsed from 233,000 tonnes a year in 1963, the peak, to 1,000 tonnes in 2021, roughly 16 grams per person.

That was two years after Japan had left the International Convention for the Regulation of Whaling, the treaty under which the IWC operates, and resumed commercial whaling. Commercial, here, does not mean economically sustainable. The government was spending around ¥5bn (\$35m) a year to keep the industry running in 2019. Spread over the 335 whales the Japanese fleet caught in 2019 that amounts to slightly less than ¥15m per whale. Since the exit Kyodo Senpaku, the whaling company, has had the hard job of demonstrating that commercial whaling is still a viable industry. In 2022 it managed to \phr{eke out}\mmr{to get or achieve sth with great difficulty} a small operating profit of ¥132m, which management attributed to cost-cutting. But in 2024 it will have to start making payments on ¥3bn it borrowed from the government after subsidies ended.

\begin{figure*}[tbh!]
  \includegraphics[width=0.8\paperwidth]{kyodo}
  \caption{Kyodo Senpaku news. Source: \href{https://www.kyodo-senpaku.co.jp/}{kyodo-senpaku.co.jp}}
  \label{fig:kyodo}
\end{figure*}

One \stt{这一段用总分法展开。学习段落展开是写作的重要一环} way to see this \phr{propping up}\mmr{support, help} of the whaling industry is through the lens\mmr{one way to see sth is through the lens of\ldots} of ecosystem services. An accounting system used by the UN divides these services into four kinds: provisioning, regulating, supporting and cultural. For provisioning services\mmr{在这里断句，脑补一个逗号} think of cutting timber\mmr{原木} and mining coal; hunting and gathering the bounty of the natural world. Regulating means keeping natural processes ticking over\mmr{to run or proceed in a steady but slow way}; bees pollinating flowers or freshwater plants purifying water.\stt{46-48行的几个分词句型都没有谓语动词，这叫 absolute constructions。语法上和主句没联系，靠语义串联} Supporting services are more fundamental and include the operation of carbon cycles, creation of atmospheric oxygen or the maintenance of the soil that ultimately keeps all plants and animals alive. And then there are cultural services. Nature provides people with the symbols and language to understand the world as well as food, fibre and fuel to survive it.

Whales provide cultural services in abundance. Ancient and indigenous literature often treats them as the guardians or descendants of the gods; in modern works of literature, such as ``Whale Fall''\mmr{\url{https://granta.com/whale-fall/}} by Rebecca Giggs
\marginpar{%
\captionsetup{type=figure}
\includegraphics[width=0.85\marginparwidth]{pics/rebecca}
\caption{Rebecca Giggs, photo by \href{https://www.rebeccagiggs.com/}{Sophie Davidson}, 2022}
\label{fig:rebecca}
}%
, whales die elegiacally\mmr{showing sadness} and thereby remind humanity of its cruelty. A first edition copy of ``Moby Dick'' has an auction price of over £53,000, more than the roughly ¥8.4m (\$60,000) worth of meat that\mmr{which} Mr Kubo reckons is typically on\mmr{on 指长着\ldots(重量)的肉} a whale.\mmr{Mr Kubo reckons that roughly ¥8.4m (\$60,000) worth of meat is typically on a whale}

In Japan, part of the cultural service whales provide is as a means of self-assertion. ``It is a way of raising a fist to foreigners,'' says Kurasawa Nanami of Iruka and Kujira Action Network, a whale and dolphin conservation charity. You do not have to be fond of whale meat, or partake in it at all, to hate the idea of Western influence telling the Japanese what they should do, she points out.\mmr{一种民族情感受伤式的逆反} Resentment at\mmr{words expressing feelings are usually followed by ``at'', i.e. get angry/mad \textbf{at} someone} Western sentimentality and hypocrisy over whales---did not MacArthur rebuild the industry? Were not Commodore Perry's ``black ships'' in search of a refuelling station for America's whalers?\mmr{两个问句指出日本人所坚守的捕鲸事业和技术，正由其所讨厌的西方人发明}---is\mmr{主语是 resentment and hypocrisy, 日本人微妙的心理} said to have deep roots. Conservationist critics read the story differently, highlighting the degree to which Japanese industrial whaling is an invention of the 1950s and a product of foreign influence rather than indigenous culture to boot. Cultural value can be complicated.\mmr{“自古以来”，“韩国申遗”， 比比皆是，一声叹息。}

\begin{figure*}[th!]
  \includegraphics[width=0.8\paperwidth]{blackship}
  \caption{An 1854 Japanese print depicting the expedition. The Perry Expedition (Japanese: {\jptxt 黒船来航, くろふねらいこう} \emph{\textsf{kurofune raikō}}, ``Arrival of the Black Ships'') was a diplomatic and military expedition in two separate voyages (1852–1853 and 1854–1855) to the Tokugawa shogunate by warships of the United States Navy.  The goals of this expedition included exploration, surveying, and the establishment of diplomatic relations and negotiation of trade agreements with various nations of the region. Opening contact with the government of Japan was considered a top priority of the expedition, and was one of the key reasons for its inception.  The expedition was commanded by Commodore Matthew Calbraith Perry, under orders from President Millard Fillmore.  \wiki{https://en.wikipedia.org/wiki/Perry_Expedition}}
  \label{fig:blackship}
  \end{figure*}


It can also be a wasting asset. The government spends ¥340m a year helping with whale marketing, largely to persuade younger consumers that whale is worth eating. The clients at a whale-meat vending machine in Keikyu, a Tokyo suburb, are largely older. The shop manager thinks they are nostalgic for the days when whale was served as part of school lunches. Marketing materials reassure younger customers: the walls of the shop are covered with \glspl{placards}\mmr{large signs put up in the shop} suggesting recipes, including whale spaghetti, lauding the health benefits of whale meat and suggesting that hunting whale is good for the environment: whales are eating too many salmon and squid, the posters argue. In ecosystem-services speak, the whales represent a loss; eating them has a value.

It is an argument few ecologists find convincing. But some species do have a disproportionate effect on the shape and texture of the ecosystems they inhabit. The reintroduction of wolves to Yellowstone National Park in America has been championed as having helped reduce the impact of elk\mmr{麋鹿} on trees and improve the overall health of the habitat. Such supporting services, though, cannot be easily priced on a piecemeal basis.

Dieter Helm, an Oxford economist (like Harold Salvesen) and the author of a book on natural capital, argues that such services cannot be apportioned out to individual creatures; the presence of wolves may matter a lot, but that cannot be used to put a value on the marginal wolf. For him it makes more sense to think about habitats and ecosystems as the basic unit of natural capital than particular creatures, or even particular species. ``It is absurd to ask `What is a whale worth?' '' he argues.\mmr{呼应标题}

In this view the value of ``charismatic megafauna''
\marginpar{%
\captionsetup{type=figure}
\includegraphics[width=\marginparwidth]{pics/megafauna}
\caption{Charismatic megafauna are animal species that are large with symbolic value or widespread popular appeal, and are often used by environmental activists to gain public support for environmentalist goals. Pictured is an elephant (Loxodonta africana), an example of charismatic megafauna. \wiki{https://en.wikipedia.org/wiki/Charismatic_megafauna}}
\label{fig:magafauna}
}%
---a term first used of giant pandas---is as figureheads. The ecosystem may be the thing that matters, but it is not the thing that inspires wonder, and most of its participants are barely even visible. There is no great American novel written about phytoplankton. They do not appear in cave paintings, no recording of their plaintive calls has breached the charts;\mmr{呼应前面提到的专辑, on page \refln{l:267}} they play their crucial role in the carbon cycle and the ocean food web, unsinging and unsung.\mmr{Unlike whales, these little but important creatures are not making sounds (unsinging) and therefore have not been sung/praised (unsung) by people.}

And yet the whales are not just ambassadors. Mr Chami's assessment of their ecosystem-services value rests on the idea that they play a distinctive regulatory role in carbon sequestration.
\marginpar{%
\captionsetup{type=figure}
\includegraphics[width=\marginparwidth]{sequestration}
\caption{Carbon sequestration. Source: \href{https://environmentat5280.org/du-env-blog/should-colorado-really-sequester-carbon}{environmentat5280.org}}
\label{fig:sequestration}
}%
They increase the size of the ecosystem they inhabit, and thus its ability to absorb carbon. Their vertical movement through the water column returns nutrients from the lower tiers to the surface waters in ``buoyant faecal plumes'', thus allowing more phytoplankton to grow. The study of this ``whale pump'' dates back to the Discovery expeditions. Next is the ``whale conveyor belt''. Migratory whales move nutrients horizontally, as well as vertically, getting them to places which continental run-off\mmr{流入河流中的雨水} and ocean currents\mmr{洋流} neglect. Finally there is whale fall: the descent of \glspl{carcasses}\mmr{dead animal bodies}, with their carbon, into the abyss.

Based on the stimulus whales provide to the ocean's ability to sequester carbon, Mr Chami estimates that returning whale populations to their pre-whaling levels would allow the fertilised oceans to store away 1.7bn more tonnes of carbon dioxide a year than today's depleted ones do. At a carbon price of \$60 per tonne—a fairly conservative estimate of what economists call the ``social cost of carbon''—that represents a benefit to the world at large of around \$13 per person per year through improved regulation of ecosystem services. Whales, then, are global public goods: undervalued by the market and therefore undersupplied.\mmr{so convincing}
