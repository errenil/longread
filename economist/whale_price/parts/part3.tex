\dropf{I}{T WAS THE} needs of the whalers, not the whales, which drove change\mmr{To value whales is still to think about whales, not about their needs. Topic sentence.}. The Great Depression saw the world with more whale oil than consumers wanted. Unilever, a British consumer-goods company and the world’s largest buyer of the stuff, said that it would buy up the excess stock—but only if the Norwegian fleet sat out the 1931-1932 hunting season. The following year the British and Norwegian industries decided to self-regulate, setting quotas for catches so that prices would rise. The companies' quotas were measured in ``blue-whale units''\mmr{See \reffig{fig:whales} for relations between different whales}, each equivalent to a single blue whale, two fin whales, five humpback whales or 12 sei whales. Salvesen, the whaler-economist, bought his rivals' quotas, paying them to keep their ships at home while maximising the capital efficiency of his own.

\begin{figure*}[ht!]
  \centering
\includegraphics[width=\linewidth]{pics/whales}
\caption{Species of whales.  Source: \href{https://www.britannica.com/animal/cetacean/images-videos}{Britannica}}
\label{fig:whales}
\end{figure*}

Prices went up. So did profits. But there was an unforeseen consequence.\mmr{topic sentence} Giving all blue whales the same value encouraged whalers to seek out the largest of them, which meant the cows\mmr{an adult female of large animals}, especially pregnant ones storing up blubber for later conversion to milk. In 1932-33 the whalers working under the quota system took 422 cows for every 100 bulls\mmr{an adult male of some large animals}. They were profiting today at the expense of the future in a peculiarly literal way\mmr{Literally speaking, they were indeed 杀鸡取卵}.

The end of the second world war saw a new internationalism take hold. The International Whaling Commission (IWC) was founded "to provide for the proper conservation of whale stocks and thus make possible the orderly development of the whaling industry''. Its early decades were an \phr{abject failure}\mmr{extremely unsuccessful}. Alongside the long-standing whaling giants of Norway and Britain\mmr{Who once a killer of whales is now a protector?}, other nations were joining the hunt. General Douglas MacArthur,
\marginpar{
\captionsetup{type=figure}
\includegraphics[width=\marginparwidth]{pics/macarthur}
\caption{MacArthur in 1945. Source: \href{https://en.wikipedia.org/wiki/Douglas_MacArthur\#/media/File:MacArthur_Manila_(cropped2).jpg}{Wikipedia}}
\label{fig:arthur}
}%
who administered the American occupation of Japan, encouraged the defeated and semi-starving country to use its decommissioned navy ships to hunt whales in the Southern Ocean.

The Soviet Union, too, became a major whaling nation, with the help of American whaling ships provided through the wartime lend-lease programme and a German factory ship seized as \gls{reparations}\mmr{money paid by a defeated country after a war}. Joseph Stalin, the Soviet leader, encouraged IWC delegates to conserve whales from ``predatory and irrational exploitation''. The logic of scientific socialism would, the USSR thought, do better than free markets at preserving natural resources. It didn't.\mmr{Funny. Stalin was once an advocate of whale protection? Is he a true one? Of course not. All is politics!}

The IWC was not completely \gls{supine}\mmr{软弱的}. From 1955 on it imposed limits on the catch of the largest whales, the blues. But overall it continued to grow. In 1964, roughly the industry's peak, an estimated 82,000 whales were slaughtered\mmr{kill animals for their meat}. Blues and rights being protected and scarce, sperm whales \phr{bore the brunt of}\mmr{bear the brunt of sth 首当其冲} the hunt.

In that decade, though, whales, and in particular endangered ones, started to take on a new symbolic and cultural value. Big-eyed, warm-blooded and viciously slaughtered, they served well as standins\mmr{symbols} for peace and the environment in general at a time when young people were increasingly exercised\mmr{put into action} about both. The growth of Soviet whaling added some cold war needle to the mix\mmr{践行和平与环保的年轻一代开始登上历史舞台; needle: hostility, 意指当时捕鲸(苏联)和护鲸的两方势力间针锋相对，有了冷战的感觉}.

The cold war bolstered the whales' standing\mmr{position/importance} in another way, too. After the second world war, the whaling industry adapted sonar systems and helicopters from their military uses to whale spotting. In the 1960s new ways of underwater listening developed by American engineers listening out for Soviet submarines made the first recordings of humpback-whale song. Scientists took note; so, soon, did the public. ``The Songs of the Humpback Whale'',
\marginpar{%
\captionsetup{type=figure}
\includegraphics[width=\marginparwidth]{pics/humpback}
\caption{The groundbreaking 1970 album Songs of the Humpback Whale, by Roger Payne and Scott McVay, led to a conservation movement to save the marine creatures from extinction. Source: \href{https://mauinow.com/2021/02/08/a-whale-of-a-tale-roger-paynes-album-of-humpback-songs-still-resonates-50-years-later/}{mauinow.com}}
\label{fig:humpback}
}%
an LP\mmr{long-playing}
\marginpar{%
\captionsetup{type=figure}
\includegraphics[width=\marginparwidth]{pics/lp}
\caption{A 12-inch LP vinyl record (黑胶唱片). Source: \href{https://en.wikipedia.org/wiki/LP_record\#/media/File:12in-Vinyl-LP-Record-Angle.jpg}{Wikipedia}}
\label{fig:lp}
}%
released in 1970 at a retail price of \$9.95, breached the Billboard top 200 the following year, reaching number 176 in May 1971 and staying in contention for eight weeks.\label{l:267} The whales received no royalties, but their \gls{plaintive}\mmr{sad} calls provided great PR\mmr{Music PR, or public relations, is a form of promotion for the artist’s music, tours, or artist brand in general, that generates media (and ultimately public) attention. (\src{https://soundcharts.com/blog/music-pr}{soundcharts.come})}.

America's Endangered Species Conservation Act of 1969 included protection for the eight largest whales. In 1971 the country proposed an outright moratorium on whaling at the IWC. At the UN's 1972 Stockholm conference on the environment, forebear of today's COP climate summits, the American delegation proposed and won a vote calling for a ten-year moratorium on commercial whaling. When the USSR and Japan rejected the whaling quotas the IWC assigned them for being too limited, President Gerald Ford threatened a trade embargo.

Countries without any prior history or economic interest in whaling began to join the IWC, keen for the diplomatic boost from being on the side of nature. Steadily, it turned from a whaling organisation into an anti-whaling one. The eventual moratorium on commercial whaling was passed at the IWC in 1982.

Overall, the 20th century saw an estimated 2.9m great whales killed; Phillip Clapham, an American biologist, called it ``the largest hunt in human history''. Many species were \phr{on the verge of extinction}\mmr{濒临灭绝}.

To a large extent, the IWC ban worked. Whale populations are recovering, albeit slowly. The IWC estimated that there were roughly 450 blue whales left in the Southern Ocean at the time of the moratorium; its latest estimate, made at the turn of the millennium, was that there were now 2,300. That is a rate of growth of about 8.5\% a year. Humpback whales have done even better: numbers in the south Antarctic have increased from 10,230 to 42,000.\mmr{Note the change in the number of whales and their prices} The environmental movement had scored what might be seen as its first global success. But there were still a few places where whales had a price.\mmr{Attracting readers' attention and curiosity}
